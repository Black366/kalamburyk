﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;

namespace Kalambury_Klient
{
    public partial class Form1 : Form
    {
        private Pen myBrush;
        private SolidBrush myBrush2;
        private Graphics myGraphics;
        private bool isDrawing = false;
        private List<string> receiveBuff = new List<string>();
        private char[] zanki = { '@', '#', '$', '%', '^', '&', '*', '(', ')' };
        int x = 0, y = 0;

        public Form1()
        {
            InitializeComponent();

        }
        Socket sck;

        byte[] receivedBuf = new byte[1024];
        Thread thr;

        private void button4_Click(object sender, EventArgs e)
        {
            //if (textBox6.Text=="[0-9])
            //thr = new Thread(LoopConnect);
            //thr.IsBackground = true;
            //thr.Start();
            if (!Regex.IsMatch(textBox6.Text, @"^0*(25[0-5]|2[0-4]\d|1?\d\d?)(\.0*(25[0-5]|2[0-4]\d|1?\d\d?)){3}$"))
            {
                MessageBox.Show("Nieprawidłowy adres IP");
                textBox6.Focus();
            }
            else
            {
                LoopConnect();

                try
                {
                    sck.BeginReceive(receivedBuf, 0, receivedBuf.Length, SocketFlags.None, new AsyncCallback(ReceiveData), sck);
                    byte[] buffer = Encoding.UTF8.GetBytes("@" + textBox5.Text);
                    sck.Send(buffer);
                }
                catch
                {
                    MessageBox.Show("Serwer nie osiągalny");
                    button4.Text = ("Połącz");
                    button4.Enabled = true;
                }
            }
        }

        private void LoopConnect()
        {
            button4.Invoke(new Action(delegate ()
            {
                sck = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                int attempts = 0;
                button4.Text = ("Łączenie");
                button4.Enabled = false;
                while (!sck.Connected)
                {
                    try
                    {
                        if (attempts >= 20)
                            break;
                        attempts++;
                        sck.Connect(IPAddress.Parse(textBox6.Text), 100);
                    }
                    catch (SocketException)
                    {
                        //Console.Clear();
                        //attempts++;
                        button4.Text = (attempts.ToString());
                    }
                }
                if (sck.Connected)
                {
                    button4.Text = ("Połączono!");
                    button4.Enabled = false;
                    button3.Enabled = true;
                }
                else
                {
                    button4.Text = ("Połącz");
                    button4.Enabled = true;
                }
            }));

        }
        string test = "";
       
        private void ReceiveData(IAsyncResult ar)
        {
            try
            {

                Socket socket = (Socket)ar.AsyncState;
                int received = socket.EndReceive(ar);
                byte[] dataBuf = new byte[received];
                Array.Copy(receivedBuf, dataBuf, received);
                string s = (Encoding.UTF8.GetString(dataBuf));
                string tmp = "";

                for (int i = 0; i < s.Length; i++)
                {
                    if (!zanki.Contains(s[i]) || i == 0)
                    {
                        tmp += s[i];
                        if (i == s.Length - 1)
                        {
                            receiveBuff.Add(tmp);
                            tmp = "";
                        }
                    }

                    else if (zanki.Contains(s[i]) && i != 0)
                    {
                        receiveBuff.Add(tmp);
                        tmp = "";
                        tmp += s[i];
                    }
                    else
                    {
                        receiveBuff.Add(tmp);
                        tmp = "";
                    }
                }

                for (int i = 0; i < receiveBuff.Count; i++)
                {
                    s = receiveBuff[i];
                    textBox2.Invoke(new Action(delegate ()
                    {
                        if (s[0] == '#')
                        {
                            string sTmp = s.Replace("#", "");
                            textBox2.Text += sTmp;
                        }
                        else if (s[0] == '%')
                        {
                            test += s;
                            rysuj(s);

                        }
                        else if (s[0] == '(')
                        {
                            string sTmp = s.Replace("(", "");
                            panel2.BackColor = Color.FromArgb(Convert.ToInt32(sTmp));
                            myBrush.Color = Color.FromArgb(Convert.ToInt32(sTmp));
                            myBrush2.Color = Color.FromArgb(Convert.ToInt32(sTmp));

                        }
                        else if (s[0] == ')')
                        {
                            string sTmp = s.Replace(")", "");
                            trackBar1.Value = Convert.ToInt32(sTmp);
                            myBrush = new Pen(panel2.BackColor, trackBar1.Value);

                        }
                        else if (s[0] == '*')
                        {
                            string sTmp = s.Replace("*", "");
                            textBox4.Text = sTmp;
                        }
                        else if (s[0] == '^')
                        {
                            string sTmp = s.Replace("^", "");
                            textBox1.Text += sTmp;
                            panel1.Enabled = true;
                            groupBox1.Enabled = true;
                            button3.Enabled = true;
                            sck.Send(Encoding.UTF8.GetBytes("$ok"));
                        }
                        else if (s[0] == '+')
                        {
                            string sTmp = s.Replace("+", "");
                            if (Convert.ToInt32(sTmp) <= 100)
                                progressBar1.Value = Convert.ToInt32(sTmp);
                        }
                        else if (s[0] == '$')
                        {
                            string sTmp = s.Replace("$", "");
                            if (sTmp == "clear")
                                myGraphics.Clear(Color.White);
                            else if (sTmp == "StopMyBrush")
                            {
                                x = 0;
                                y = 0;
                            }
                            else if (sTmp == "start")
                            {

                            }
                            else if (sTmp == "stop")
                            {
                                textBox1.Clear();
                                panel1.Enabled = false;
                                groupBox1.Enabled = false;
                                x = 0;
                                y = 0;
                                myGraphics.Clear(Color.White);

                            }

                        }
                    }));
                }
                receiveBuff.Clear();
                sck.BeginReceive(receivedBuf, 0, receivedBuf.Length, SocketFlags.None, new AsyncCallback(ReceiveData), sck);
            }
            catch
            {
                MessageBox.Show("Serwer rozłączony");
                sck.Close();
                button4.Invoke(new Action(delegate ()
                {
                    button4.Text = ("Połącz");
                    button4.Enabled = true;
                    button3.Enabled = true;
                }));

            }
        }
        private void rysuj(string s)
        {
            string[] tabS = new string[2];
            //int s = sck.Send(Encoding.Default.GetBytes($"%{panel2.BackColor} {e.X} {e.Y} {trackBar1.Value} {trackBar1.Value}%"));
            int j = 1;
            for (int i = 0; i < tabS.Length; i++)
            {

                string tmp = "";
                while (true)
                {
                    if (s[j] == ' ' || s[j] == '%')
                        break;
                    tmp += s[j];
                    j++;
                }
                tabS[i] = tmp;
                j++;
                if (j == '%' && i == 4)
                {
                    narysuj(tabS);
                    i = 0;
                }
            }
            if (x == 0 && y == 0)
            {
                x = Convert.ToInt32(tabS[0]);
                y = Convert.ToInt32(tabS[1]);
            }

            myGraphics.DrawLine(myBrush, x, y, Convert.ToInt32(tabS[0]), Convert.ToInt32(tabS[1]));
            myGraphics.FillEllipse(myBrush2, Convert.ToInt32(tabS[0]) - trackBar1.Value / 2, Convert.ToInt32(tabS[1]) - trackBar1.Value / 2, trackBar1.Value, trackBar1.Value);
            x = Convert.ToInt32(tabS[0]);
            y = Convert.ToInt32(tabS[1]);
        }

        private void narysuj(string[] tabS)
        {
            if (x == 0 && y == 0)
            {
                x = Convert.ToInt32(tabS[0]);
                y = Convert.ToInt32(tabS[1]);
            }
            myGraphics.DrawLine(myBrush, x, y, Convert.ToInt32(tabS[0]), Convert.ToInt32(tabS[1]));
            myGraphics.FillEllipse(myBrush2, Convert.ToInt32(tabS[0]) - trackBar1.Value / 2, Convert.ToInt32(tabS[1]) - trackBar1.Value / 2, trackBar1.Value, trackBar1.Value);
            x = Convert.ToInt32(tabS[0]);
            y = Convert.ToInt32(tabS[1]);
        }
        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (sck.Connected)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (textBox3.Text != "")
                    {
                        for (int i = 0; i < textBox3.Text.Length; i++)
                        {
                            if (zanki.Contains(textBox3.Text[i]))
                                textBox3.Text = textBox3.Text.Replace(textBox3.Text[i], '?');
                        }
                        int s = sck.Send(Encoding.UTF8.GetBytes('#' + textBox3.Text + "\r\n"));

                    }
                    textBox3.Clear();
                }
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            myBrush = new Pen(panel2.BackColor, trackBar1.Value);
            myGraphics = panel1.CreateGraphics();
            myBrush2 = new SolidBrush(panel2.BackColor);
        }

        private void panel2_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                panel2.BackColor = colorDialog1.Color;
                myBrush.Color = panel2.BackColor;
                myBrush2.Color = panel2.BackColor;
                int s = sck.Send(Encoding.UTF8.GetBytes($"({panel2.BackColor.ToArgb()}"));
            }
        }
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            isDrawing = true;
            int s = sck.Send(Encoding.Default.GetBytes($"({panel2.BackColor.ToArgb()}"));
            Task.Delay(1);
            s = sck.Send(Encoding.Default.GetBytes($"){trackBar1.Value} "));
            Task.Delay(1);
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {

            isDrawing = false;
            x = 0;
            y = 0;
            sck.Send(Encoding.UTF8.GetBytes("$StopMyBrush"));
        }

        private void TrackBar1_ValueChanged(object sender, System.EventArgs e)
        {
            int s = sck.Send(Encoding.UTF8.GetBytes($"){trackBar1.Value} "));
            myBrush = new Pen(panel2.BackColor, trackBar1.Value);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {

            if (isDrawing == true)
            {
                int s = sck.Send(Encoding.UTF8.GetBytes($"%{e.X} {e.Y} "));
                if (x == 0 && y == 0)
                {
                    x = e.X;
                    y = e.Y;
                }
                myGraphics.DrawLine(myBrush, x, y, e.X, e.Y);
                myGraphics.FillEllipse(myBrush2, e.X - trackBar1.Value / 2, e.Y - trackBar1.Value / 2, trackBar1.Value, trackBar1.Value);

                test += $"%{panel2.BackColor.ToArgb()} {e.X} {e.Y} {trackBar1.Value} {trackBar1.Value} ";
                x = e.X;
                y = e.Y;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            myGraphics.Clear(Color.White);
            sck.Send(Encoding.UTF8.GetBytes("$clear"));
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.SelectionStart = textBox2.Text.Length;
            textBox2.ScrollToCaret();
        }
        SaveFileDialog sfd = new SaveFileDialog();
        private void button2_Click(object sender, EventArgs e)
        {
            //sfd.Filter = "TXT|*.txt";
            //sfd.FileName = "Rozkodowany_tekst";
            //if (sfd.ShowDialog() == DialogResult.OK)
            //{
            //    StreamWriter streamWriter = new StreamWriter(sfd.FileName);
            //    streamWriter.Write(test);
            //    MessageBox.Show("Zapisano", "Informacja",
            //                     MessageBoxButtons.OK,
            //                     MessageBoxIcon.Information);
            //}
            int s = sck.Send(Encoding.UTF8.GetBytes("$res"));
            textBox1.Clear();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            int s = sck.Send(Encoding.UTF8.GetBytes($"){trackBar1.Value} "));
            myBrush = new Pen(panel2.BackColor, trackBar1.Value);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int s = sck.Send(Encoding.UTF8.GetBytes("$r"));
            button3.Enabled = false;
        }

        private void textBox3_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox3.Text == "\r\n")
                textBox3.Clear();
        }
    }
}
